package log

import "testing"

func BenchmarkDebug(b *testing.B) {
	logger.SetCurrent(DebugLevel)
	for i := 0; i < 1000; i++ {
		logger.Debug("benchmark debug", "\n")
	}
}

func BenchmarkDebugf(b *testing.B) {
	logger.SetCurrent(DebugLevel)
	for i := 0; i < 1000; i++ {
		logger.Debugf("%s\n", "benchmark debugf")
	}
}

func BenchmarkDebugln(b *testing.B) {
	logger.SetCurrent(DebugLevel)
	for i := 0; i < 1000; i++ {
		logger.Debugln("benchmark debugln")
	}
}

func BenchmarkPrint(b *testing.B) {
	logger.SetCurrent(DefaultLevel)
	for i := 0; i < 1000; i++ {
		logger.Print("benchmark print", "\n")
	}
}

func BenchmarkPrintf(b *testing.B) {
	logger.SetCurrent(DefaultLevel)
	for i := 0; i < 1000; i++ {
		logger.Printf("%s\n", "benchmark printf")
	}
}

func BenchmarkPrintln(b *testing.B) {
	logger.SetCurrent(DefaultLevel)
	for i := 0; i < 1000; i++ {
		logger.Println("benchmark println")
	}
}

func BenchmarkInfo(b *testing.B) {
	logger.SetCurrent(InfoLevel)
	for i := 0; i < 1000; i++ {
		logger.Info("benchmark info", "\n")
	}
}

func BenchmarkInfof(b *testing.B) {
	logger.SetCurrent(InfoLevel)
	for i := 0; i < 1000; i++ {
		logger.Infof("%s\n", "benchmark infof")
	}
}

func BenchmarkInfoln(b *testing.B) {
	logger.SetCurrent(InfoLevel)
	for i := 0; i < 1000; i++ {
		logger.Infoln("benchmark infoln")
	}
}

func BenchmarkWarning(b *testing.B) {
	logger.SetCurrent(WarningLevel)
	for i := 0; i < 1000; i++ {
		logger.Warning("benchmark warning", "\n")
	}
}

func BenchmarkWarningf(b *testing.B) {
	logger.SetCurrent(WarningLevel)
	for i := 0; i < 1000; i++ {
		logger.Warningf("%s\n", "benchmark warningf")
	}
}

func BenchmarkWarningln(b *testing.B) {
	logger.SetCurrent(WarningLevel)
	for i := 0; i < 1000; i++ {
		logger.Warningln("benchmark warningln")
	}
}

func BenchmarkError(b *testing.B) {
	logger.SetCurrent(ErrorLevel)
	for i := 0; i < 1000; i++ {
		logger.Error("benchmark error", "\n")
	}
}

func BenchmarkErrorf(b *testing.B) {
	logger.SetCurrent(ErrorLevel)
	for i := 0; i < 1000; i++ {
		logger.Errorf("%s\n", "benchmark errorf")
	}
}

func BenchmarkErrorln(b *testing.B) {
	logger.SetCurrent(ErrorLevel)
	for i := 0; i < 1000; i++ {
		logger.Errorln("benchmark errorln")
	}
}
