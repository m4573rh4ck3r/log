package log

import (
	"sync"
	"testing"
)

func TestRaceDebug(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(DebugLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Debug("test race debug", "\n")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceDebugf(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(DebugLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Debugf("%s\n", "test race debugf")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceDebugln(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(DebugLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Debugln("test race debugln")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRacePrint(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(DefaultLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Print("test race print", "\n")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRacePrintf(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(DefaultLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Printf("%s\n", "test race printf")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRacePrintln(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(DefaultLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Println("test race println")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceInfo(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(InfoLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Info("test race info", "\n")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceInfof(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(InfoLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Infof("%s\n", "test race infof")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceInfoln(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(InfoLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Infoln("test race infoln")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceWarning(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(WarningLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Warning("test race warning", "\n")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceWarningf(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(WarningLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Warningf("%s\n", "test race warningf")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceWarningln(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(WarningLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Warningln("test race warningln")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceError(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(ErrorLevel)
	for i := 0; i < 1100; i++ {
		wg.Add(1)
		go func() {
			logger.Error("test race error", "\n")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceErrorf(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(ErrorLevel)
	for i := 0; i < 4000; i++ {
		wg.Add(1)
		go func() {
			logger.Errorf("%s\n", "test race errorf")
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestRaceErrorln(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	logger.SetCurrent(ErrorLevel)
	for i := 0; i < 4000; i++ {
		wg.Add(1)
		go func() {
			logger.Errorln("test race errorln")
			wg.Done()
		}()
	}
	wg.Wait()
}
