#!/usr/bin/env bash

set -o pipefail
set -o nounset
set -o errexit

malformed_files=$(gofmt -s -l .)

if [ $(echo $malformed_files | sed '/^\w*/ d' | wc -l) -gt 0 ]; then echo -e "error, formatting required: \n\t$malformed_files" && exit 1; fi
