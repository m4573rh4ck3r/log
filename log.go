package log

import (
	"sync"
)

var (
	mu     sync.Mutex
	colors map[string]string = map[string]string{
		"reset":  "\033[0m",
		"red":    "\033[0;31m",
		"yellow": "\033[0;33m",
		"cyan":   "\033[0;36m",
	}
)
