package log

import (
	"fmt"
	"os"
	"runtime"
)

const (
	DebugLevel   = int64(4)
	InfoLevel    = int64(3)
	DefaultLevel = int64(2)
	WarningLevel = int64(1)
	ErrorLevel   = int64(0)
)

type Level struct {
	Current int64
}

type Logger struct {
	Level *Level
	NoColor bool
}

func (l *Logger) GetCurrent() int64 {
	return l.Level.Current
}

func (l *Logger) SetCurrent(v int64) {
	l.Level.Current = normalize(v)
}

func normalize(v int64) int64 {
	if v <= ErrorLevel {
		return ErrorLevel
	} else if v >= DebugLevel {
		return DebugLevel
	}
	return v
}

func New(lvl int64, noNoColor bool) *Logger {
	return &Logger{
		Level: &Level{
			Current: normalize(lvl),
		},
		NoColor: noNoColor,
	}
}

func (l *Logger) SetNoColor(c bool) {
	l.NoColor = c
}

func (l *Logger) Debug(args ...interface{}) {
	if l.GetCurrent() < DebugLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stdout, fmt.Sprintf("%s %s: line %d: %s", l.getDebugPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Debugf(format string, args ...interface{}) {
	if l.GetCurrent() < DebugLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stdout, fmt.Sprintf("%s %s: line %d: %s", l.getDebugPrefix(), file, line, fmt.Sprintf(format, args...)))
	mu.Unlock()
}

func (l *Logger) Debugln(args ...interface{}) {
	if l.GetCurrent() < DebugLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprintln(os.Stdout, fmt.Sprintf("%s %s: line %d: %s", l.getDebugPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Print(args ...interface{}) {
	if l.GetCurrent() < DefaultLevel {
		return
	}
	mu.Lock()
	fmt.Fprint(os.Stdout, fmt.Sprint(args...))
	mu.Unlock()
}

func (l *Logger) Printf(format string, args ...interface{}) {
	if l.GetCurrent() < DefaultLevel {
		return
	}
	mu.Lock()
	fmt.Fprint(os.Stdout, fmt.Sprint(fmt.Sprintf(format, args...)))
	mu.Unlock()
}

func (l *Logger) Println(args ...interface{}) {
	if l.GetCurrent() < DefaultLevel {
		return
	}
	mu.Lock()
	fmt.Fprintln(os.Stdout, fmt.Sprintf("%s", fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Info(args ...interface{}) {
	if l.GetCurrent() < InfoLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stdout, fmt.Sprintf("%s %s: line %d: %s", l.getInfoPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Infof(format string, args ...interface{}) {
	if l.GetCurrent() < InfoLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stdout, fmt.Sprintf("%s %s: line %d: %s", l.getInfoPrefix(), file, line, fmt.Sprintf(format, args...)))
	mu.Unlock()
}

func (l *Logger) Infoln(args ...interface{}) {
	if l.GetCurrent() < InfoLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprintln(os.Stdout, fmt.Sprintf("%s %s: line %d: %s", l.getInfoPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Warning(args ...interface{}) {
	if l.GetCurrent() < WarningLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getWarningPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Warningf(format string, args ...interface{}) {
	if l.GetCurrent() < WarningLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getWarningPrefix(), file, line, fmt.Sprintf(format, args...)))
	mu.Unlock()
}

func (l *Logger) Warningln(args ...interface{}) {
	if l.GetCurrent() < WarningLevel {
		return
	}
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprintln(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getWarningPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Error(args ...interface{}) {
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getErrorPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Errorf(format string, args ...interface{}) {
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getErrorPrefix(), file, line, fmt.Sprintf(format, args...)))
	mu.Unlock()
}

func (l *Logger) Errorln(args ...interface{}) {
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprintln(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getErrorPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
}

func (l *Logger) Fatal(args ...interface{}) {
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getFatalPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
	os.Exit(1)
}

func (l *Logger) Fatalf(format string, args ...interface{}) {
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprint(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getFatalPrefix(), file, line, fmt.Sprintf(format, args...)))
	mu.Unlock()
	os.Exit(1)
}

func (l *Logger) Fatalln(args ...interface{}) {
	mu.Lock()
	_, file, line, _ := runtime.Caller(1)

	fmt.Fprintln(os.Stderr, fmt.Sprintf("%s %s: line %d: %s", l.getFatalPrefix(), file, line, fmt.Sprint(args...)))
	mu.Unlock()
	os.Exit(1)
}

func (l *Logger) Panic(args ...interface{}) {
	panic(fmt.Sprintf("%s %s", l.getPanicPrefix(), fmt.Sprint(args...)))
}

func (l *Logger) Panicf(format string, args ...interface{}) {
	panic(fmt.Sprintf("%s %s", l.getPanicPrefix(), fmt.Sprintf(format, args...)))
}

func (l *Logger) Panicln(args ...interface{}) {
	panic(fmt.Sprintf("%s %s\n", l.getPanicPrefix(), fmt.Sprint(args...)))
}
