package log

import (
	"fmt"
)

func (l *Logger) getDebugPrefix() string {
	if ! l.NoColor {
		return fmt.Sprintf("[%sDEBUG%s] ", colors["cyan"], colors["reset"])
	}
	return "[INFO] "
}

func (l *Logger) getInfoPrefix() string {
	if ! l.NoColor {
		return fmt.Sprintf("[%sINFO%s] ", colors["cyan"], colors["reset"])
	}
	return "[INFO] "
}

func (l *Logger) getWarningPrefix() string {
	if ! l.NoColor {
		return fmt.Sprintf("[%sWARNING%s] ", colors["yellow"], colors["reset"])
	}
	return "[WARNING] "
}

func (l *Logger) getErrorPrefix() string {
	if ! l.NoColor {
		return fmt.Sprintf("[%sERROR%s] ", colors["red"], colors["reset"])
	}
	return "[ERROR] "
}

func (l *Logger) getFatalPrefix() string {
	if ! l.NoColor {
		return fmt.Sprintf("[%sFATAL%s] ", colors["red"], colors["reset"])
	}
	return "[ERROR] "
}

func (l *Logger) getPanicPrefix() string {
	if ! l.NoColor {
		return fmt.Sprintf("[%sPANIC%s] ", colors["red"], colors["reset"])
	}
	return "[ERROR] "
}
