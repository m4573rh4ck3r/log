package log

import "testing"

var (
	logger = New(2, true)
)

func TestDebug(t *testing.T) {
	logger.SetCurrent(DebugLevel)
	logger.Debug("test debug", "\n")
}

func TestDebugf(t *testing.T) {
	logger.SetCurrent(DebugLevel)
	logger.Debugf("%s\n", "test debugf")
}

func TestDebugln(t *testing.T) {
	logger.SetCurrent(DebugLevel)
	logger.Debugln("test debugln")
}

func TestPrint(t *testing.T) {
	logger.SetCurrent(DefaultLevel)
	logger.Print("test print", "\n")
}

func TestPrintf(t *testing.T) {
	logger.SetCurrent(DefaultLevel)
	logger.Printf("%s\n", "test printf")
}

func TestPrintln(t *testing.T) {
	logger.SetCurrent(DefaultLevel)
	logger.Println("test println")
}

func TestInfo(t *testing.T) {
	logger.SetCurrent(InfoLevel)
	logger.Info("test info", "\n")
}

func TestInfof(t *testing.T) {
	logger.SetCurrent(InfoLevel)
	logger.Infof("%s\n", "test infof")
}

func TestInfoln(t *testing.T) {
	logger.SetCurrent(InfoLevel)
	logger.Infoln("test infoln")
}

func TestWarning(t *testing.T) {
	logger.SetCurrent(WarningLevel)
	logger.Warning("test warning", "\n")
}

func TestWarningf(t *testing.T) {
	logger.SetCurrent(WarningLevel)
	logger.Warningf("%s\n", "test warningf")
}

func TestWarningln(t *testing.T) {
	logger.SetCurrent(WarningLevel)
	logger.Warningln("test warningln")
}

func TestError(t *testing.T) {
	logger.SetCurrent(ErrorLevel)
	logger.Error("test error", "\n")
}

func TestErrorf(t *testing.T) {
	logger.SetCurrent(ErrorLevel)
	logger.Errorf("%s\n", "test errorf")
}

func TestErrorln(t *testing.T) {
	logger.SetCurrent(ErrorLevel)
	logger.Errorln("test errorln")
}

func TestFatal(t *testing.T) {
	defer func() {
		err := recover()
		if err != nil {
			t.Fatal("failed to recover from fatal")
		}
	}()

	logger.Fatal("test fatal", "\n")
}

func TestFatalf(t *testing.T) {
	defer func() {
		err := recover()
		if err != nil {
			t.Fatal("failed to recover from fatalf")
		}
	}()

	defer recover()

	logger.Fatalf("test %s\n", "fatalf")
}

func TestFatalln(t *testing.T) {
	defer func() {
		err := recover()
		if err != nil {
			t.Fatal("failed to recover from fatalln")
		}
	}()

	defer recover()

	logger.Fatalln("test fatalln")
}
