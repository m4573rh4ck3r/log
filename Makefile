SHELL=/usr/bin/env bash

project_name=log
repository=gitlab.com/m4573rh4ck3r/log
coverage_file=.testCoverage.txt
testdir=./...
bindir=$(CURDIR)/bin

ifeq ($(CC),cc)
	CC=go
endif

.SUFFIXES:
.SUFFIXES: .go

.PHONY: all
all: test ## run tests

$(bindir):
	@mkdir -p $@

$(bindir)/%: | $(bindir)

$(bindir)/golint:
	@GO111MODULE=off GOBIN=$(bindir) go get -u golang.org/x/lint/golint

.PHONY: lint
lint: | $(bindir)/golint ## run golint
	@$(bindir)/golint -set_exit_status ./...

.PHONY: distclean
distclean: ## cleanup build dependencies
	@$(CC) clean -x -cache

.PHONY: mod-clean
mod-clean: ## cleanup mod dependencies
	@$(CC) clean -x -modcache

.PHONY: test-clean
test-clean: ## cleanup test cache
	@$(CC) clean -x -testcache

.PHONY: clean
clean: distclean mod-clean test-clean ## remove the entire cache

.PHONY: test
test: ## run tests
	@$(CC) test -parallel 6 -v -run TestDebug* $(testdir) | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'
	@$(CC) test -parallel 6 -v -run TestPrint* $(testdir) | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'
	@$(CC) test -parallel 6 -v -run TestInfo* $(testdir) | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'
	@$(CC) test -parallel 6 -v -run TestWarning* $(testdir) | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'
	@$(CC) test -parallel 6 -v -run TestError* $(testdir) | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'
	@$(CC) test -parallel 6 -v -run TestFatal* $(testdir) | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'

.PHONY: bench
bench: ## run benchmarks
	@$(CC) test -bench=. -run Benchmark* $(testdir)

.PHONY: race
race: ## run tests with race detection
	@$(CC) test -parallel 1 -v -race -run TestRace* $(testdir)

.PHONY: coverage
coverage: ## run tests with coverage
	@$(CC) test -parallel 6 -v -cover $(testdir)

.PHONY: fmt
fmt: ## format all files
	@gofmt -l -w -s .
	@goimports -w -format-only -local "$(repository)" .

.PHONY: get
get: ## get dependencies
	@$(CC) mod download

.PHONY: update-deps
update-deps: ## update dependencies
	@$(CC) get -u ./...

.PHONY: tidy
tidy: ## tidy up dependencies
	@$(CC) mod tidy

.PHONY: check-fmt
check-fmt: ## check if all files are formatted correctly
	@hack/check-fmt.sh

.PHONY: info
info: ## display this info
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<configurations> <target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "   \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
